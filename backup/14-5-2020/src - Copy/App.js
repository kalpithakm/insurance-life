import React, { Component } from "react";
import "./App.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";
import loader from './loader.gif';

class App extends Component {
  constructor(props) {
    super(props);
    this.uploadSelfie = this.uploadSelfie.bind(this);

     jQuery(function(){
    var select =  jQuery(".ageSelect");
    for (var i=0;i<=120;i++){
        select.append( jQuery('<option></option>').val(i).html(i))
    }
});
  }



   onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
    console.log("takePhoto");
 
    jQuery.ajax({
      url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
      method: "POST",
      crossDomain: true,
     
      headers: {
        Authorization: "Token b8b332467b1994253e4252448264e8b3946d3fb1",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        dataUri: dataUri,
        attributes: "age,gender,smoker,ethnicity,alcohol,bmi,region,education,marital,flipVertical"
       
      }),
      success: function(response) {
        console.log("response");

        if (response.face) {

          if (response.face.length === 1) {

            var text = new Object();
            text.gender = response.face[0].faceAttributes.gender;
            text.age = response.face[0].faceAttributes.age;
            text.smoker = response.face[0].faceAttributes.smoker;
            text.alcohol = response.face[0].faceAttributes.alcohol;
            text.bmi = response.face[0].faceAttributes.bmi.label;
            text.ethnicity = response.face[0].faceAttributes.ethnicity.label;
            text.region = response.face[0].faceAttributes.region.label ;
            text.faceid = response.face[0].faceId;
      

            if (text.gender != null) {
              jQuery("input[name=gender][value=" + text.gender + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=smoker][value=" + text.smoker + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=alcohol][value=" + text.alcohol + "]").attr(
                "checked",
                true
              );
            }

           if (text.age) {
              var inputtwo = text.age;
              
              var input_range_1 = "1-5";
              var input_range_2 = "6-10";

              var input_range_3 = "11-15";
              var input_range_4 = "16-20";

              var input_range_5 = "21-25";
              var input_range_6 = "26-30";

              var input_range_7 = "31-35";
              var input_range_8 = "36-40";

              var input_range_9 = "41-45";
              var input_range_10 = "46-50";

              var input_range_11 = "51-55";
              var input_range_12 = "56-60";

              var input_range_13 = "61-65";
              var input_range_14 = "66-70";

              var input_range_15 = "71-75";
              var input_range_16 = "76-80";

              var input_range_17 = "81-85";
              var input_range_18 = "86-90";

              var input_range_19 = "91-95";
              var input_range_20 = "96-100";
              if(text.age <= 5)
              {
                document.getElementById("years").value = input_range_1;
                console.log(input_range_1);
              }
              else if(text.age <= 10)
              {
                document.getElementById("years").value = input_range_2;
                console.log(input_range_2);
              }
              else if(text.age <= 15)
              {
                document.getElementById("years").value = input_range_3;
                console.log(input_range_3);
              }
              else if(text.age <= 20)
              {
                document.getElementById("years").value = input_range_4;
                console.log(input_range_4);
              }
              else if(text.age <= 25)
              {
                document.getElementById("years").value = input_range_5;
                console.log(input_range_5);
              }
              else if(text.age <= 30)
              {
                document.getElementById("years").value = input_range_6;
                console.log(input_range_6);
              }
              else if(text.age <= 35)
              {
                document.getElementById("years").value = input_range_7;
                console.log(input_range_7);
              }
              else if(text.age <= 40)
              {
                document.getElementById("years").value = input_range_8;
                console.log(input_range_8);
              }
               else if(text.age <= 45)
              {
                document.getElementById("years").value = input_range_9;
                console.log(input_range_9);
              }
              else if(text.age <= 50)
              {
                document.getElementById("years").value = input_range_10;
                console.log(input_range_10);
              }

               else if(text.age <= 55)
              {
                document.getElementById("years").value = input_range_11
                console.log(input_range_11);
              }
              else if(text.age <= 60)
              {
                document.getElementById("years").value = input_range_12;
                console.log(input_range_12);
              }
              else if(text.age <= 65)
              {
                document.getElementById("years").value = input_range_13;
                console.log(input_range_13);
              }
              else if(text.age <= 70)
              {
                document.getElementById("years").value = input_range_14;
                console.log(input_range_14);
              }
              else if(text.age <= 75)
              {
                document.getElementById("years").value = input_range_15;
                console.log(input_range_15);
              }
              else if(text.age <= 80)
              {
                document.getElementById("years").value = input_range_16;
                console.log(input_range_16);
              }
              else if(text.age <= 85)
              {
                document.getElementById("years").value = input_range_17;
                console.log(input_range_17);
              }
              else if(text.age <= 90)
              {
                document.getElementById("years").value = input_range_18;
                console.log(input_range_18);
              }
               else if(text.age <= 90)
              {
                document.getElementById("years").value = input_range_19;
                console.log(input_range_19);
              }
              else if(text.age <= 100)
              {
                document.getElementById("years").value = input_range_20;
                console.log(input_range_20);
              }
             
            }

            if (text.ethnicity) {
              var inputthree = text.ethnicity;
              document.getElementById("ethnicSelect").value = inputthree;
              
            }

            if (text.bmi) {
              var inputfour = text.bmi;
              document.getElementById("bmiselect").value = inputfour;
              
            }

             if (text.region) {
              var inputfive = text.region;
              document.getElementById("regionselect").value = inputfive;
            
            }

            if (text.gender === "female") {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            }
            if (text.gender === "male") {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            }

            jQuery("#female").click(function() {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            });

            jQuery("#male").click(function() {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            });

            if (text.smoker === true) {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            }

            if (text.smoker === false) {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            }

            jQuery("#tobacco-y").click(function() {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            });

            jQuery("#tobacco-n").click(function() {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            });

            jQuery("#submit").click(function() {
              var fid = text.faceid;
              var fgender = jQuery(".gender:checked").val();
              var fage = document.getElementById("years").value;
              var fethnic = document.getElementById("ethnicSelect").value;
              var fbmi = document.getElementById("bmiselect").value;
              var fregion = document.getElementById("regionselect").value;
              var fsmoker = jQuery(".smoker:checked").val();

              jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/face/update/",
                headers: {
                  Authorization:
                    "Token b8b332467b1994253e4252448264e8b3946d3fb1",
                  "Content-Type": "application/json"
                },
                method: "POST",
                crossDomain: true,
                data: JSON.stringify({
                  faceId: fid,
                  gender: fgender,
                  smoker: fsmoker,
                  age: fage,
                  ethnicity: fethnic,
                  bmi: fbmi,
                  region:fregion
                }),

                success: function(response) {
                  if (response) {
                    
                    var codeString = JSON.stringify(response, null, 4);
                    console.log(codeString);
                    jQuery("#show-more").show();
                    jQuery("#show-faceid").html(fid);
                  }
                },

                error: function(response) {
                  document.getElementById("error-message-update").innerHTML =
                    "<p>Please try again..</p>";
                }
              });
            });

            document.getElementById("error-message").style.display = "none";
            document.getElementById("tab").style.display = "block";
            document.getElementById("video-section").style.display = "none";
            document.getElementById("camera-head").style.display = "none";
            document.getElementById("upload-photo-section").style.display =
              "none";
          } else {
            document.getElementById("error-message").innerHTML =
              "<p>Please try again, selfie should include only one face</p>";
          }
        } else {
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, face not found</p>";
        }
      },

      beforeSend: function()
      {
       jQuery('.loader').show();
       jQuery('#container-circles').hide();
       jQuery('.react-html5-camera-photo').hide();

      },
      complete: function()
      {
       jQuery('.loader').hide();
       jQuery('#container-circles').show();
       jQuery('.react-html5-camera-photo').show();
      },

      error: function(response) {
     

        if (response !== null) {
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, server error</p>";
        } else {
          console.log("error");

          document.getElementById("error-message").innerHTML =
            "<p>The server didn’t respond,Please try again</p>";
        }

         
      }
    });
  }

    uploadSelfie(e) 
  {
    console.log("upload");
    document.getElementById("tab").style.display = "none";

var canvas=document.getElementById("canvas");
var ctx=canvas.getContext("2d");
var dataUri;
var input = document.getElementById('input');
input.addEventListener('change', handleFiles);
function handleFiles(e) 
{
  var img = new Image;
  img.onload = function() {
   document.getElementById("canvas-sec").style.display = "block";
    ctx.drawImage(img,0,0,canvas.width,canvas.height);
    dataUri = canvas.toDataURL("image/jpeg");
    console.log("url", dataUri);
  
   jQuery.ajax({
       url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
      method: "POST",
      crossDomain: true,
      headers: {
        Authorization: "Token b8b332467b1994253e4252448264e8b3946d3fb1",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        dataUri: dataUri,
        attributes: "age,gender,smoker,ethnicity,alcohol,bmi,region,education,marital"
       
      }),
      success: function(response) {
        console.log("response");

        if (response.face) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

          if (response.face.length === 1) {
            var text = new Object();
        
            text.gender = response.face[0].faceAttributes.gender;
            text.age = response.face[0].faceAttributes.age;
            text.smoker = response.face[0].faceAttributes.smoker;
            text.alcohol = response.face[0].faceAttributes.alcohol;
            text.bmi = response.face[0].faceAttributes.bmi.label;
            text.ethnicity = response.face[0].faceAttributes.ethnicity.label;
            text.region = response.face[0].faceAttributes.region.label ;
            text.faceid = response.face[0].faceId;
      

            if (text.gender != null) {
              jQuery("input[name=gender][value=" + text.gender + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=smoker][value=" + text.smoker + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=alcohol][value=" + text.alcohol + "]").attr(
                "checked",
                true
              );
            }

            if (text.age) {
              var inputtwo = text.age;
              
              var input_range_1 = "1-5";
              var input_range_2 = "6-10";

              var input_range_3 = "11-15";
              var input_range_4 = "16-20";

              var input_range_5 = "21-25";
              var input_range_6 = "26-30";

              var input_range_7 = "31-35";
              var input_range_8 = "36-40";

              var input_range_9 = "41-45";
              var input_range_10 = "46-50";

              var input_range_11 = "51-55";
              var input_range_12 = "56-60";

              var input_range_13 = "61-65";
              var input_range_14 = "66-70";

              var input_range_15 = "71-75";
              var input_range_16 = "76-80";

              var input_range_17 = "81-85";
              var input_range_18 = "86-90";

              var input_range_19 = "91-95";
              var input_range_20 = "96-100";
              if(text.age <= 5)
              {
                document.getElementById("years").value = input_range_1;
                console.log(input_range_1);
              }
              else if(text.age <= 10)
              {
                document.getElementById("years").value = input_range_2;
                console.log(input_range_2);
              }
              else if(text.age <= 15)
              {
                document.getElementById("years").value = input_range_3;
                console.log(input_range_3);
              }
              else if(text.age <= 20)
              {
                document.getElementById("years").value = input_range_4;
                console.log(input_range_4);
              }
              else if(text.age <= 25)
              {
                document.getElementById("years").value = input_range_5;
                console.log(input_range_5);
              }
              else if(text.age <= 30)
              {
                document.getElementById("years").value = input_range_6;
                console.log(input_range_6);
              }
              else if(text.age <= 35)
              {
                document.getElementById("years").value = input_range_7;
                console.log(input_range_7);
              }
              else if(text.age <= 40)
              {
                document.getElementById("years").value = input_range_8;
                console.log(input_range_8);
              }
               else if(text.age <= 45)
              {
                document.getElementById("years").value = input_range_9;
                console.log(input_range_9);
              }
              else if(text.age <= 50)
              {
                document.getElementById("years").value = input_range_10;
                console.log(input_range_10);
              }

               else if(text.age <= 55)
              {
                document.getElementById("years").value = input_range_11
                console.log(input_range_11);
              }
              else if(text.age <= 60)
              {
                document.getElementById("years").value = input_range_12;
                console.log(input_range_12);
              }
              else if(text.age <= 65)
              {
                document.getElementById("years").value = input_range_13;
                console.log(input_range_13);
              }
              else if(text.age <= 70)
              {
                document.getElementById("years").value = input_range_14;
                console.log(input_range_14);
              }
              else if(text.age <= 75)
              {
                document.getElementById("years").value = input_range_15;
                console.log(input_range_15);
              }
              else if(text.age <= 80)
              {
                document.getElementById("years").value = input_range_16;
                console.log(input_range_16);
              }
              else if(text.age <= 85)
              {
                document.getElementById("years").value = input_range_17;
                console.log(input_range_17);
              }
              else if(text.age <= 90)
              {
                document.getElementById("years").value = input_range_18;
                console.log(input_range_18);
              }
               else if(text.age <= 90)
              {
                document.getElementById("years").value = input_range_19;
                console.log(input_range_19);
              }
              else if(text.age <= 100)
              {
                document.getElementById("years").value = input_range_20;
                console.log(input_range_20);
              }
             
            }

            if (text.ethnicity) {
              var inputthree = text.ethnicity;
              document.getElementById("ethnicSelect").value = inputthree;
              
            }

            if (text.bmi) {
              var inputfour = text.bmi;
              document.getElementById("bmiselect").value = inputfour;
              
            }

             if (text.region) {
              var inputfive = text.region;
              document.getElementById("regionselect").value = inputfive;
            
            }

            if (text.gender === "female") {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            }
            if (text.gender === "male") {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            }

            jQuery("#female").click(function() {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            });

            jQuery("#male").click(function() {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            });

            if (text.smoker === true) {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            }

            if (text.smoker === false) {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            }

            jQuery("#tobacco-y").click(function() {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            });

            jQuery("#tobacco-n").click(function() {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            });

            jQuery("#submit").click(function() {
              var fid = text.faceid;
              var fgender = jQuery(".gender:checked").val();
              var fage = document.getElementById("years").value;
              var fethnic = document.getElementById("ethnicSelect").value;
              var fbmi = document.getElementById("bmiselect").value;
              var fregion = document.getElementById("regionselect").value;
              var fsmoker = jQuery(".smoker:checked").val();

              jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/face/update/",
                headers: {
                  Authorization:
                    "Token b8b332467b1994253e4252448264e8b3946d3fb1",
                  "Content-Type": "application/json"
                },
                method: "POST",
                crossDomain: true,
                data: JSON.stringify({
                  faceId: fid,
                  gender: fgender,
                  smoker: fsmoker,
                  age: fage,
                  ethnicity: fethnic,
                  bmi: fbmi,
                  region:fregion
                }),

                success: function(response) {
                  if (response) {
                    
                    var codeString = JSON.stringify(response, null, 4);
                    console.log(codeString);
                    jQuery("#show-more").show();
                    jQuery("#show-faceid").html(fid);
                  }
                },

                error: function(response) {
                  ctx.clearRect(0, 0, canvas.width, canvas.height);
                  document.getElementById("error-message-update").innerHTML =
                    "<p>Please try again..</p>";
                }
              });
            });

            document.getElementById("error-message").style.display = "none";
            document.getElementById("tab").style.display = "block";
            document.getElementById("video-section").style.display = "none";
            document.getElementById("camera-head").style.display = "none";
            document.getElementById("upload-photo-section").style.display =
              "none";
          } else {
            document.getElementById("error-message").innerHTML =
              "<p>Please try again, selfie should include only one face</p>";
          }
        } else {
           ctx.clearRect(0, 0, canvas.width, canvas.height);
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, face not found</p>";

        }
      },

       beforeSend: function()
      {
       jQuery('.upload-loader').show()
      },
      complete: function()
      {
       jQuery('.upload-loader').hide();
      },

      error: function(response) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if (response !== null) {
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, server error</p>";
        } else {
          console.log("error");

          document.getElementById("error-message").innerHTML =
            "<p>The server didn’t respond,Please try again</p>";
        }
      }
    }); 

  };
  img.src = URL.createObjectURL(e.target.files[0]);
}
  }

  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
   
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  render() {
    return (
      <div className="App">
        <div className="header" id="camera-head">
          <h2>TAKE A SELFIE TO BUY POLICY</h2>
        </div>
         
        <div id="video-section" style={{ display: "none" }}>
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 600 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.50}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />

           <div className="loader"> </div>

          <div id="tips">
          <p>
           Note: Please face a light source, align your face, take off your glasses, and tuck your hair behind your ears.           
          </p>
          </div>

          <div>
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section">
          <div className="ios-pic">
            <div id="canvas-sec" style={{ display: "none" }}>
              <canvas id="canvas" width="300" height="300"></canvas>
            </div>

             <div className="upload-loader">  </div>

            <div>
               <label className="fileContainer">
                Use a Photo
                   <input type="file" id="input" onClick={this.uploadSelfie}/>
              </label>
             
            </div>
            
          </div>
        </div>
        
         <div>
        <p id="error-message" />
         </div>

        <div id="tab" style={{ display: "none" }}>
          <div className="inner-header">
            <h2>Face Predictions for insurance PREMIUM</h2>
          </div>
          <Container>
            <Row className="tab-row">
              <Col>
                <div id="gender" className="qn-ans-group">
                  <h5 className="qn-title">Gender</h5>
                  <div className="ans-cont js-radio-change">
                    <div className="icon-group">
                      <span
                        className="char-icon icon-male"
                        style={{ display: "none" }}
                      />
                      <span
                        className="char-icon icon-female"
                        style={{ display: "none" }}
                      />
                    </div>

                    <div className="bs-radio">
                      <input
                        id="male"
                        type="radio"
                        defaultValue="male"
                        name="gender"
                        required
                        className="gender"
                      />
                      <label htmlFor="male" className="form-label">
                        <span className="icon-name">Male</span>
                      </label>
                    </div>
                    <div className="bs-radio">
                      <input
                        id="female"
                        type="radio"
                        defaultValue="female"
                        name="gender"
                        className="gender"
                      />
                      <label htmlFor="female" className="form-label">
                        <span className="icon-name">Female</span>
                      </label>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div id="tobacco" className="qn-ans-group">
                  <h5 className="qn-title"> Consumes Tobacco </h5>
                  <div className="ans-cont js-radio-change">
                    <div className="icon-group">
                      <span
                        className="char-icon icon-tobacco-y"
                        style={{ display: "none" }}
                      />
                      <span
                        className="char-icon icon-tobacco-n"
                        style={{ display: "none" }}
                      />
                    </div>

                    <div className="bs-radio">
                      <input
                        id="tobacco-y"
                        type="radio"
                        defaultValue="true"
                        name="smoker"
                        className="smoker"
                        required
                      />
                      <label htmlFor="tobacco-y" className="form-label">
                        <span className="icon-name">Yes</span>
                      </label>
                    </div>
                    <div className="bs-radio">
                      <input
                        id="tobacco-n"
                        type="radio"
                        defaultValue="false"
                        className="smoker"
                        name="smoker"
                      />
                      <label htmlFor="tobacco-n" className="form-label">
                        <span className="icon-name">No</span>
                      </label>
                    </div>
                  </div>
                </div>
              </Col>

                <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">visual age**</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="years" className="form-label">
                        <span className="char-icon icon-cake" />
                        &nbsp;
                      </label>
                      <div className="age-para"></div>
                      <Input type="text" className="ageSelect" name="select" id="years">
                      </Input>

                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

            </Row>

            <Row>
            

              <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">Ethnicity</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="ethnic-people" className="form-label">
                        <span className="char-icon icon-ethnic" />
                        &nbsp;
                      </label>

                      <Input type="select" name="select" id="ethnicSelect">
                        <option>white</option>
                        <option>black</option>
                        <option>asian</option>
                        <option>indian</option>
                        <option>other</option>
                      </Input>
                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">BMI</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="bmi-people" className="form-label">
                        <span className="char-icon icon-bmi" />
                        &nbsp;
                      </label>

                      <Input type="select" name="bmiselect" id="bmiselect">
                        <option>underweight</option>
                        <option>healthy</option>
                        <option>overweight</option>
                        <option>obese</option>
                      </Input>
                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

                 <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">Region</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="region-people" className="form-label">
                        <span className="char-icon icon-region" />
                        &nbsp;
                      </label>

                      <Input type="select" name="regionselect" id="regionselect">
                        <option>bengal</option>
                        <option>northeast</option>
                        <option>northcentralwest</option>
                        <option>south</option>
                        <option>none</option>
                      </Input>
                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

            </Row>

            <Row>
              <Col>
                <p id="error-message-update"></p>
              </Col>
            </Row>

            <div className="back-btn">
             <p>* Our demo ends here..</p>
             <p>** Visual age is how old a person looks like rather than the actual biological age.</p>
             
              <div>
                <Button href="https://insurance.sparshik.com/">Back </Button>
               
                <div id="show-more" style={{ display: "none" }}>
                  <h6>
                    Updated: <span id="show-faceid"></span>
                  </h6>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default App;
