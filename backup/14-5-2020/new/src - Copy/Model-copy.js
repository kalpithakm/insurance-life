import React, { Component } from "react";
import "./Model.css";
import "./custom.css";
import Camera, { FACING_MODES, IMAGE_TYPES } from "react-html5-camera-photo";
import "react-html5-camera-photo/build/css/index.css";
import jQuery from "jquery";
import { Container, Row, Col, Button, Input } from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

class Model extends Component {
  constructor(props) {
    super(props);
    this.uploadSelfie = this.uploadSelfie.bind(this);
  }

  onTakePhoto(dataUri) {
    // Do stuff with the dataUri photo...
    console.log("takePhoto");
    console.log(dataUri);

    jQuery.ajax({
      url: "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
      method: "POST",
      crossDomain: true,
      headers: {
        Authorization: "Token 269b74c3a124b3709ce0036b00494b2b11459a67",
        "Content-Type": "application/json"
      },
      data: JSON.stringify({
        dataUri: dataUri,
        attributes: "age,gender,smoker,ethnicity,alcohol,bmi",
        recognitionModel: "model_02"
      }),
      contentType: "application/json",
      success: function(response) {
        console.log("response");

        if (response.face) {
          if (response.face.length === 1) {
            var text = new Object();

            text.gender = response.face[0].faceAttributes.gender;
            text.age = response.face[0].faceAttributes.age;
            text.smoker = response.face[0].faceAttributes.smoker;
            text.alcohol = response.face[0].faceAttributes.alcohol;
            text.bmi = response.face[0].faceAttributes.bmi.max;
            text.ethnicity = response.face[0].faceAttributes.ethnicity.max;
            text.faceid = response.face[0].faceId;
            console.log(text.gender);
            console.log(text.age);
            console.log(text.smoker);
            console.log(text.alcohol);
            console.log(text.ethnicity);
            console.log(text.bmi);
            console.log(text.faceid);

            if (text.gender != null) {
              jQuery("input[name=gender][value=" + text.gender + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=smoker][value=" + text.smoker + "]").attr(
                "checked",
                true
              );
            }

            if (text.smoker != null) {
              jQuery("input[name=alcohol][value=" + text.alcohol + "]").attr(
                "checked",
                true
              );
            }

            if (text.age) {
              var inputtwo = text.age;
              document.getElementById("years").value = inputtwo;
              console.log(inputtwo);
              /* this.setState({agevalue: inputtwo });*/
            }

            if (text.ethnicity) {
              var inputthree = text.ethnicity;
              document.getElementById("ethnicSelect").value = inputthree;
              console.log(inputthree);
            }

            if (text.bmi) {
              var inputfour = text.bmi;
              document.getElementById("bmiselect").value = inputfour;
              console.log(inputfour);
            }

            if (text.gender === "female") {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            }
            if (text.gender === "male") {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            }

            jQuery("#female").click(function() {
              jQuery(".icon-female").show();
              jQuery(".icon-male").hide();
            });

            jQuery("#male").click(function() {
              jQuery(".icon-male").show();
              jQuery(".icon-female").hide();
            });

            if (text.smoker === true) {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            }

            if (text.smoker === false) {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            }

            jQuery("#tobacco-y").click(function() {
              jQuery(".icon-tobacco-y").show();
              jQuery(".icon-tobacco-n").hide();
            });

            jQuery("#tobacco-n").click(function() {
              jQuery(".icon-tobacco-n").show();
              jQuery(".icon-tobacco-y").hide();
            });

            if (text.alcohol === true) {
              jQuery(".icon-alcohol-y").show();
              jQuery(".icon-alcohol-n").hide();
            }

            if (text.alcohol === false) {
              jQuery(".icon-alcohol-n").show();
              jQuery(".icon-alcohol-y").hide();
            }

            jQuery("#alcohol-y").click(function() {
              jQuery(".icon-alcohol-y").show();
              jQuery(".icon-alcohol-n").hide();
            });

            jQuery("#alcohol-n").click(function() {
              jQuery(".icon-alcohol-n").show();
              jQuery(".icon-alcohol-y").hide();
            });

            jQuery("#submit").click(function() {
              var fid = text.faceid;
              var fgender = jQuery(".gender:checked").val();
              var fage = document.getElementById("years").value;
              var fethnic = document.getElementById("ethnicSelect").value;
              var fbmi = document.getElementById("bmiselect").value;
              var fsmoker = jQuery(".smoker:checked").val();
              var falcohol = jQuery(".alcohol:checked").val();
              console.log(fid);
              console.log(fgender);
              console.log(fage);
              console.log(fsmoker);
              console.log(fethnic);
              console.log(fbmi);
              console.log(falcohol);

              jQuery.ajax({
                url:
                  "https://southeastasia.cognitive.sparshik.com/api/v1/face/update/",
                headers: {
                  Authorization:
                    "Token 269b74c3a124b3709ce0036b00494b2b11459a67",
                  "Content-Type": "application/json"
                },
                method: "POST",
                crossDomain: true,
                data: JSON.stringify({
                  faceId: fid,
                  gender: fgender,
                  smoker: fsmoker,
                  age: fage,
                  ethnicity: fethnic,
                  alcohol: falcohol,
                  bmi: fbmi
                }),

                success: function(response) {
                  if (response) {
                    
                    var codeString = JSON.stringify(response, null, 4);
                    console.log(codeString);
                    jQuery("#show-more").show();
                    jQuery("#show-faceid").html(fid);
                  }
                },

                error: function(response) {
                  document.getElementById("error-message-update").innerHTML =
                    "<p>Please try again..</p>";
                }
              });
            });

            document.getElementById("error-message").style.display = "none";
            document.getElementById("tab").style.display = "block";
            document.getElementById("video-section").style.display = "none";
            document.getElementById("camera-head").style.display = "none";
            document.getElementById("upload-photo-section").style.display =
              "none";
          } else {
            document.getElementById("error-message").innerHTML =
              "<p>Please try again, selfie should include only one face</p>";
          }
        } else {
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, face not found</p>";
        }
      },
      error: function(response) {
        if (response !== null) {
          document.getElementById("error-message").innerHTML =
            "<p>Please try again, server error</p>";
        } else {
          console.log("error");

          document.getElementById("error-message").innerHTML =
            "<p>Please try again, server error</p>";
        }
      }
    });
  }

  uploadSelfie(e) {
    document.getElementById("tab").style.display = "none";

    /* document.getElementById("upload-photo-section").style.display = "block";*/
    jQuery(function($) {
      function readFile() {
        if (this.files && this.files[0]) {
          var ImgfileReader = new FileReader();

          ImgfileReader.addEventListener("load", function(e) {
            document.getElementById("photo").src = e.target.result;
            var imageUri = e.target.result;

            $.ajax({
              url:
                "https://southeastasia.cognitive.sparshik.com/api/v1/face/detect/",
              method: "POST",
              crossDomain: true,
              headers: {
                Authorization: "Token 269b74c3a124b3709ce0036b00494b2b11459a67",
                "Content-Type": "application/json"
              },
              data: JSON.stringify({
                dataUri: imageUri,
                attributes: "age,gender,smoker,alcohol,ethnicity,bmi",
                recognitionModel: "model_02"
              }),
              contentType: "application/json",
              success: function(response) {
                console.log("response");

                if (response.face) {
                  if (response.face.length === 1) {
                    var text = new Object();
                    text.gender = response.face[0].faceAttributes.gender;
                    text.age = response.face[0].faceAttributes.age;
                    text.smoker = response.face[0].faceAttributes.smoker;
                    text.alcohol = response.face[0].faceAttributes.alcohol;
                    text.ethnicity =
                      response.face[0].faceAttributes.ethnicity.max;
                    text.bmi = response.face[0].faceAttributes.bmi.max;
                    text.faceid = response.face[0].faceId;
                    console.log(text.gender);
                    console.log(text.age);
                    console.log(text.smoker);
                    console.log(text.alcohol);
                    console.log(text.ethnicity);
                    console.log(text.bmi);
                    console.log(text.faceid);

                    if (text.gender != null) {
                      jQuery(
                        "input[name=gender][value=" + text.gender + "]"
                      ).attr("checked", true);
                    }

                    if (text.smoker != null) {
                      jQuery(
                        "input[name=smoker][value=" + text.smoker + "]"
                      ).attr("checked", true);
                    }

                    if (text.smoker != null) {
                      jQuery(
                        "input[name=alcohol][value=" + text.alcohol + "]"
                      ).attr("checked", true);
                    }

                    if (text.age) {
                      var inputtwo = text.age;
                      document.getElementById("years").value = inputtwo;
                      console.log(inputtwo);
                      /*that.setState({agevalue: inputtwo });*/
                    }

                    if (text.ethnicity) {
                      var inputthree = text.ethnicity;
                      document.getElementById(
                        "ethnicSelect"
                      ).value = inputthree;
                      console.log(inputthree);
                    }

                    if (text.bmi) {
                      var inputfour = text.bmi;
                      document.getElementById("bmiselect").value = inputfour;
                      console.log(inputfour);
                    }

                    if (text.gender === "female") {
                      jQuery(".icon-female").show();
                      jQuery(".icon-male").hide();
                    }
                    if (text.gender === "male") {
                      jQuery(".icon-male").show();
                      jQuery(".icon-female").hide();
                    }

                    jQuery("#female").click(function() {
                      jQuery(".icon-female").show();
                      jQuery(".icon-male").hide();
                    });

                    jQuery("#male").click(function() {
                      jQuery(".icon-male").show();
                      jQuery(".icon-female").hide();
                    });

                    if (text.smoker === true) {
                      jQuery(".icon-tobacco-y").show();
                      jQuery(".icon-tobacco-n").hide();
                    }

                    if (text.smoker === false) {
                      jQuery(".icon-tobacco-n").show();
                      jQuery(".icon-tobacco-y").hide();
                    }

                    jQuery("#tobacco-y").click(function() {
                      jQuery(".icon-tobacco-y").show();
                      jQuery(".icon-tobacco-n").hide();
                    });

                    jQuery("#tobacco-n").click(function() {
                      jQuery(".icon-tobacco-n").show();
                      jQuery(".icon-tobacco-y").hide();
                    });

                    if (text.alcohol === true) {
                      jQuery(".icon-alcohol-y").show();
                      jQuery(".icon-alcohol-n").hide();
                    }

                    if (text.alcohol === false) {
                      jQuery(".icon-alcohol-n").show();
                      jQuery(".icon-alcohol-y").hide();
                    }

                    jQuery("#alcohol-y").click(function() {
                      jQuery(".icon-alcohol-y").show();
                      jQuery(".icon-alcohol-n").hide();
                    });

                    jQuery("#alcohol-n").click(function() {
                      jQuery(".icon-alcohol-n").show();
                      jQuery(".icon-alcohol-y").hide();
                    });

                    jQuery("#submit").click(function() {
                      var fid = text.faceid;
                      var fgender = jQuery(".gender:checked").val();
                      var fage = document.getElementById("years").value;
                      var fethnic = document.getElementById("ethnicSelect")
                        .value;
                      var fbmi = document.getElementById("bmiselect").value;
                      var fsmoker = jQuery(".smoker:checked").val();
                      var falcohol = jQuery(".alcohol:checked").val();
                      console.log(fid);
                      console.log(fgender);
                      console.log(fage);
                      console.log(fsmoker);
                      console.log(fethnic);
                      console.log(fbmi);
                      console.log(falcohol);

                      jQuery.ajax({
                        url:
                          "https://southeastasia.cognitive.sparshik.com/api/v1/face/update/",
                        headers: {
                          Authorization:
                            "Token 269b74c3a124b3709ce0036b00494b2b11459a67",
                          "Content-Type": "application/json"
                        },
                        method: "POST",
                        crossDomain: true,
                        data: JSON.stringify({
                          faceId: fid,
                          gender: fgender,
                          smoker: fsmoker,
                          age: fage,
                          ethnicity: fethnic,
                          alcohol: falcohol,
                          bmi: fbmi
                        }),

                        success: function(response) {
                          if (response) {
                           
                            var codeString = JSON.stringify(response, null, 4);
                            console.log(codeString);
                            jQuery("#show-more").show();
                            jQuery("#show-faceid").html(fid);
                          }
                        },

                        error: function(response) {
                          document.getElementById(
                            "error-message-update"
                          ).innerHTML = "<p>Please try again..</p>";
                        }
                      });
                    });

                    document.getElementById("video-section").style.display =
                      "none";
                    document.getElementById("error-message").style.display =
                      "none";
                    document.getElementById("tab").style.display = "block";
                    document.getElementById(
                      "upload-photo-section"
                    ).style.display = "none";
                    document.getElementById("camera-head").style.display =
                      "none";
                  } else {
                    document.getElementById("error-message").innerHTML =
                      "<p>Please try again, selfie should include only one face</p>";
                  }
                } else {
                  document.getElementById("error-message").innerHTML =
                    "<p>Please try again, face not found</p>";
                }
              },
              error: function(response) {
                if (response !== null) {
                  document.getElementById("error-message").innerHTML =
                    "<p>Please try again, server error</p>";
                } else {
                  console.log("error");

                  document.getElementById("error-message").innerHTML =
                    "<p>Please try again, server error</p>";
                }
              }
            });
          });

          ImgfileReader.readAsDataURL(this.files[0]);
        }
      }

      document
        .getElementById("browse-file")
        .addEventListener("change", readFile);
    });
  }

  onCameraError(error) {
    console.error("onCameraError");
    document.getElementById("video-section").style.display = "none";
    this.uploadSelfie();
  }

  onCameraStart(stream) {
    console.log("onCameraStart");
  }

  onCameraStop() {
    console.log("onCameraStop");
  }

  render() {
    return (
      <div className="Model">
        <div className="header" id="camera-head">
          <h2>TAKE A SELFIE TO BUY POLICY</h2>
        </div>

        <div id="video-section">
          <Camera
            onTakePhoto={dataUri => {
              this.onTakePhoto(dataUri);
            }}
            onCameraError={error => {
              this.onCameraError(error);
            }}
            idealFacingMode={FACING_MODES}
            idealResolution={{ width: 900, height: 600 }}
            imageType={IMAGE_TYPES.JPG}
            imageCompression={0.97}
            isMaxResolution={false}
            isImageMirror={false}
            isDisplayStartCameraError={true}
            sizeFactor={1}
            onCameraStart={stream => {
              this.onCameraStart(stream);
            }}
            onCameraStop={() => {
              this.onCameraStop();
            }}
          />
          <div>
            <p className="or">OR</p>
          </div>
        </div>

        <div id="upload-photo-section">
          <div className="ios-pic">
            <div>
              <img className="display-img" id="pic" src="" alt="" />
              <img className="display-img-url" id="photo" src="" alt="" />
            </div>
            <div>
              <label className="fileContainer">
                Use a Photo
                <input
                  id="browse-file"
                  type="file"
                  onClick={this.uploadSelfie}
                />
              </label>
            </div>
          </div>
        </div>

        <p id="error-message" />

        <div id="tab" style={{ display: "none" }}>
          <div className="inner-header">
            <h2>Face Predictions for insurance PREMIUM</h2>
          </div>
          <Container>
            <Row className="tab-row">
              <Col>
                <div id="gender" className="qn-ans-group">
                  <h5 className="qn-title">Gender</h5>
                  <div className="ans-cont js-radio-change">
                    <div className="icon-group">
                      <span
                        className="char-icon icon-male"
                        style={{ display: "none" }}
                      />
                      <span
                        className="char-icon icon-female"
                        style={{ display: "none" }}
                      />
                    </div>

                    <div className="bs-radio">
                      <input
                        id="male"
                        type="radio"
                        defaultValue="male"
                        name="gender"
                        required
                        className="gender"
                      />
                      <label htmlFor="male" className="form-label">
                        <span className="icon-name">Male</span>
                      </label>
                    </div>
                    <div className="bs-radio">
                      <input
                        id="female"
                        type="radio"
                        defaultValue="female"
                        name="gender"
                        className="gender"
                      />
                      <label htmlFor="female" className="form-label">
                        <span className="icon-name">Female</span>
                      </label>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div id="tobacco" className="qn-ans-group">
                  <h5 className="qn-title">Smoking Preference </h5>
                  <div className="ans-cont js-radio-change">
                    <div className="icon-group">
                      <span
                        className="char-icon icon-tobacco-y"
                        style={{ display: "none" }}
                      />
                      <span
                        className="char-icon icon-tobacco-n"
                        style={{ display: "none" }}
                      />
                    </div>

                    <div className="bs-radio">
                      <input
                        id="tobacco-y"
                        type="radio"
                        defaultValue="true"
                        name="smoker"
                        className="smoker"
                        required
                      />
                      <label htmlFor="tobacco-y" className="form-label">
                        <span className="icon-name">Smoking</span>
                      </label>
                    </div>
                    <div className="bs-radio">
                      <input
                        id="tobacco-n"
                        type="radio"
                        defaultValue="false"
                        className="smoker"
                        name="smoker"
                      />
                      <label htmlFor="tobacco-n" className="form-label">
                        <span className="icon-name">Non Smoking</span>
                      </label>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div id="alcoholic" className="qn-ans-group">
                  <h5 className="qn-title"> Alcohol Preference</h5>
                  <div className="ans-cont js-radio-change">
                    <div className="icon-group">
                      <span
                        className="char-icon icon-alcohol-y"
                        style={{ display: "none" }}
                      />
                      <span
                        className="char-icon icon-alcohol-n"
                        style={{ display: "none" }}
                      />
                    </div>
                    <div className="bs-radio">
                      <input
                        id="alcohol-y"
                        type="radio"
                        defaultValue="true"
                        name="alcohol"
                        className="alcohol"
                        required
                      />
                      <label htmlFor="alcohol-y" className="form-label">
                        <span className="icon-name">Alcoholic</span>
                      </label>
                    </div>
                    <div className="bs-radio">
                      <input
                        id="alcohol-n"
                        type="radio"
                        defaultValue="false"
                        className="alcohol"
                        name="alcohol"
                      />
                      <label htmlFor="alcohol-n" className="form-label">
                        <span className="icon-name">Non Alcoholic</span>
                      </label>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
              <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">Age</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="years" className="form-label">
                        <span className="char-icon icon-cake" />
                        &nbsp;
                      </label>

                      <Input type="select" name="select" id="years">
                        <option>18</option>
                        <option>19</option>
                        <option>20</option>
                        <option>21</option>
                        <option>22</option>
                        <option>23</option>
                        <option>24</option>
                        <option>25</option>
                        <option>26</option>
                        <option>27</option>
                        <option>28</option>
                        <option>29</option>
                        <option>30</option>
                        <option>31</option>
                        <option>32</option>
                        <option>33</option>
                        <option>34</option>
                        <option>35</option>
                        <option>36</option>
                        <option>37</option>
                        <option>38</option>
                        <option>39</option>
                        <option>40</option>
                        <option>41</option>
                        <option>42</option>
                        <option>43</option>
                        <option>44</option>
                        <option>45</option>
                        <option>46</option>
                        <option>47</option>
                        <option>48</option>
                        <option>49</option>
                        <option>50</option>
                        <option>51</option>
                        <option>52</option>
                        <option>53</option>
                        <option>54</option>
                        <option>55</option>
                        <option>56</option>
                        <option>57</option>
                        <option>58</option>
                        <option>59</option>
                        <option>60</option>
                      </Input>

                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">Ethnicity</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="ethnic-people" className="form-label">
                        <span className="char-icon icon-ethnic" />
                        &nbsp;
                      </label>

                      <Input type="select" name="select" id="ethnicSelect">
                        <option>white</option>
                        <option>black</option>
                        <option>asian</option>
                        <option>indian</option>
                        <option>other</option>
                      </Input>
                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>

              <Col>
                <div className="qn-ans-group">
                  <h5 className="qn-title">BMI</h5>
                  <div className="ans-cont">
                    <div className="form-group typ-bg">
                      <label htmlFor="bmi-people" className="form-label">
                        <span className="char-icon icon-bmi" />
                        &nbsp;
                      </label>

                      <Input type="select" name="bmiselect" id="bmiselect">
                        <option>underweight</option>
                        <option>healthy</option>
                        <option>overweight</option>
                        <option>obese</option>
                      </Input>
                      <span className="error">
                        <p id="name_error" />
                      </span>
                    </div>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
              <Col>
                <p id="error-message-update"></p>
              </Col>
            </Row>

            <div className="back-btn">
              <p>* Our demo ends here..</p>
              <div>
                <Button href="https://insurance.sparshik.com/">Back </Button>
                <Button id="submit"> Confirm </Button>
                <div id="show-more" style={{ display: "none" }}>
                  <h6>
                    Updated: <span id="show-faceid"></span>
                  </h6>
                </div>
              </div>
            </div>
          </Container>
        </div>
      </div>
    );
  }
}

export default Model;
